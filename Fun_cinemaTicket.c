#include <stdio.h>

int fun_atndnce(int);
int fun_rev(int);
int fun_cost(int);
int fun_profit(int);

int main()
{
    int tp;
    printf("Enter the ticket price=");
    scanf("%d",&tp);

    printf("The profit is %d.\n",fun_profit(tp));
    return 0;

}
int fun_atndnce(int tp)
{
   return (120-((tp-15)*4));
}
int fun_rev(int tp)
{
    return (fun_atndnce(tp)*tp);
}
int fun_cost(int tp)
{
    return (fun_atndnce(tp)*3+500);
}
int fun_profit(int tp)
{
    return (fun_rev(tp)- fun_cost(tp));
}
